package org.gitlab.kaiserollofdoom;

import org.gitlab.kaiserollofdoom.inception.InceptionRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class MlaamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MlaamApplication.class, args);
	}

}
