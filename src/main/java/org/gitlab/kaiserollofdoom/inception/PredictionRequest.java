package org.gitlab.kaiserollofdoom.inception;

/**
 * Created by mrkaiser on 5/17/17.
 */
public class PredictionRequest {

    public PredictionRequest(){

    }

    public PredictionRequest(String url) {
        this.url = url;
    }

    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url){ this.url = url;}
}
