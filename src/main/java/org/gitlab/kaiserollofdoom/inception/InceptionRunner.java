package org.gitlab.kaiserollofdoom.inception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tensorflow.*;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by mrkaiser on 6/23/17.
 */
@Component
public class InceptionRunner {

    @Autowired
    private InceptionProperties icp;

    private Graph inceptionGraph;
    private List<String> imageLabels;

    @PostConstruct
    public void setupInceptionRunner() throws IOException {
        this.inceptionGraph = new Graph();
        this.inceptionGraph.importGraphDef(Files.readAllBytes(Paths.get(icp.getGraph())));
        this.imageLabels = imageLabels();
    }

    private List<String> imageLabels() throws IOException {
        List<String> labels;

        // Labels are 1 label per line
        // So stream through the file by lines and collect to a list
        try (Stream<String> fileInputStream = Files.lines(Paths.get(icp.getLabels()))) {
            labels = fileInputStream.collect(Collectors.toList());
        }
        return labels;
    }

    public Tensor image2Tensor(byte[] imageBytes) {
        try (Graph g = new Graph()) {
            // how do we even start
            // they preprocessed with scaling down to 224x224 and then
            // value-mean/scale
            InceptionImageGraphBuilder gb = new InceptionImageGraphBuilder(g);
            final int H = 224;
            final int W = 224;
            final float mean = 117f;
            final float scale = 1f;
            final Output input = gb.constant("input", imageBytes);
            final Output preProcessedImageGraph = gb.preProcessImage(input, H, W, mean, scale);
                    try(Session graphSession = new Session(g)){
                        return graphSession.runner().fetch(preProcessedImageGraph.op().name()).run().get(0);
                    }
        }
    }

    PredictionResponse executeInceptionGraph(byte[] imageBytes) {

        Tensor image = this.image2Tensor(imageBytes);
        Tensor labelProbabilities;
        try (Session s = new Session(inceptionGraph)) {
            labelProbabilities = s.runner().feed("input", image).fetch("output").run().get(0);

            float[] imageProbablities = labelProbabilities.copyTo(new float[1][(int) labelProbabilities.shape()[1]])[0];
            // Get the labels for the imageProbablities
            return getLabels(imageProbablities);

        }
    }

    public PredictionResponse getLabels(float[] imageProbablities) {
        List<Integer> indices = new ArrayList<>();
        // find max of imageProbabilites
        int best = 0;
        for (int i = 1; i < imageProbablities.length; i++) {
            if (imageProbablities[i] > imageProbablities[best]) {
                best = i;
            }
        }
        PredictionResponse pr = new PredictionResponse(imageLabels.get(best), imageProbablities[best]);
        return pr;
    }

    private class InceptionImageGraphBuilder {
        private Graph g;

        public InceptionImageGraphBuilder(Graph g) {
            this.g = g;
        }

        public Output binaryOp(String type, Output in_x, Output in_y) {
            return g.opBuilder(type, type).addInput(in_x).addInput(in_y).build().output(0);
        }

        Output div(Output x, Output y) {
            return binaryOp("Div", x, y);
        }

        Output sub(Output x, Output y) {
            return binaryOp("Sub", x, y);
        }

        Output resizeBilinear(Output images, Output size) {
            // commonly used resize technique
            return binaryOp("ResizeBilinear", images, size);
        }

        Output decodeJpeg(Output imageContents, long channels) {
            // take the image contents (which will be a constant on the graph with the image bytes
            // decode the 3 channel
            return g.opBuilder("DecodeJpeg", "DecodeJpeg").
                    addInput(imageContents).setAttr("channels", channels).build().output(0);
        }

        Output cast(Output value, DataType type) {
            // cast all the values  to the given data type
            return g.opBuilder("Cast", "Cast").addInput(value).setAttr("DstT", type).build().output(0);
        }

        Output expandDims(Output input, Output dim) {
            // expand the dimensions
            return binaryOp("ExpandDims", input, dim);
        }

        Output constant(String name, Object value) {
            // try to take the object and create a tensor
            try (Tensor t = Tensor.create(value)) {
                // if possible add the value with the given name as a constant on the graph
                return g.opBuilder("Const", name).setAttr("dtype", t.dataType()).setAttr("value", t).build().output(0);
            }
        }

        Output resizeImage(Output imageContents, int height, int width) {
            return this.resizeBilinear(this.expandDims(this.cast(this.decodeJpeg(imageContents, 3), DataType.FLOAT),
                    this.constant("make_batch", 0)), this.constant("size", new int[]{height, width}));
        }

        Output preProcessImage(Output imageContents, int height, int width, float mean, float scale){
                    // By the end of this you're going to wish Java had operator overloading
                    // Remember it's (value - mean)/scale
                    Output value = resizeImage(imageContents, height, width);
                    Output preProcessedImage = this.div(this.sub(value, this.constant("mean", mean)),
                            this.constant("scale", scale));
                    return preProcessedImage;
                }



    }
}