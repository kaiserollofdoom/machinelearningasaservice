package org.gitlab.kaiserollofdoom.inception;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by mrkaiser on 7/18/17.
 */
@Component
@ConfigurationProperties("inception")
public class InceptionProperties {

    private String graph;
    private String labels;

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }
}
