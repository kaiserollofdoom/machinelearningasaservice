package org.gitlab.kaiserollofdoom.inception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created by mrkaiser on 5/15/17.
 */
@RestController
class InceptionController {
    @Autowired
    InceptionRunner irg;

    @RequestMapping(method = RequestMethod.POST, path = "/api/image")
    public PredictionResponse image2Prediction(@RequestBody PredictionRequest pr) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedImage image = ImageIO.read(new URL(pr.url));
        ImageIO.write(image, "jpg", baos );
        byte[] imageBytes = baos.toByteArray();
        PredictionResponse result = irg.executeInceptionGraph(imageBytes);
        return result;
    }


}
