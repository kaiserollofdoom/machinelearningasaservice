package org.gitlab.kaiserollofdoom.inception;

/**
 * Created by mrkaiser on 5/27/17.
 */
public class PredictionResponse {
    private String label;
    private float probablities;

    public PredictionResponse(String label,float probablities) {
        this.label = label;
        this.probablities = probablities;
    }

    public String getLabel() {
        return label;
    }

    public float getProbablities() {
        return probablities;
    }
}
