package org.gitlab.kaiserollofdoom.inception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.gitlab.kaiserollofdoom.MlaamApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * Created by mrkaiser on 5/15/17.
 */


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MlaamApplication.class)
@WebAppConfiguration
public class TestInceptionController {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private InceptionController inceptionController;

    @Autowired
    ObjectMapper om;

    @Before
    public void setUp(){
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testPianoImage() throws Exception {
        //Setup piano Image url
        PredictionRequest prRequest = new PredictionRequest("http://www.discountmusicaloutlet.net/images/Digital%20Grand%20Piano.jpg");
        String pr = om.writeValueAsString(prRequest);
        //mock the call to the request api
        MvcResult mvcResult = mockMvc.perform(post("/api/image").content(pr).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        assertEquals("{\"label\":\"grand piano\",\"probablities\":0.9138464}", result);
    }
    

}
